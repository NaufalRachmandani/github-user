package com.naufal.githubuser.data.source.local

import android.content.Context
import android.content.res.AssetManager
import com.google.gson.Gson
import com.naufal.githubuser.data.source.local.model.ResponseGithubUser
import com.naufal.githubuser.data.source.local.model.UsersItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocalDataSource @Inject constructor(private val context: Context) {

    private var userList: MutableList<UsersItem> = mutableListOf()

    private fun AssetManager.readAssetsFile(fileName: String): String =
        open(fileName).bufferedReader().use { it.readText() }

    suspend fun getGithubUserList(): List<UsersItem> {
        return withContext(Dispatchers.IO) {
            val gson = Gson()
            val responseGithubUser = gson.fromJson(
                context.assets.readAssetsFile("githubuser.json"),
                ResponseGithubUser::class.java
            )
            userList.clear()
            userList.addAll(responseGithubUser.users)
            responseGithubUser.users
        }
    }

    suspend fun getSearchUsers(query: String): List<UsersItem> {
        return withContext(Dispatchers.IO) {
            val filteredUsers = userList.filter {
                it.name?.contains(query) ?: false || it.username?.contains(query) ?: false
            }
            filteredUsers
        }
    }
}