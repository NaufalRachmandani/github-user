package com.naufal.githubuser.data

import com.naufal.githubuser.data.source.local.model.UsersItem

interface GithubRepository {
    suspend fun getGithubUserList(): List<UsersItem>
    suspend fun getSearchUsers(query: String): List<UsersItem>
}