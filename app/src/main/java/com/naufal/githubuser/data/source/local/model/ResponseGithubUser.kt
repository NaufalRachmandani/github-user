package com.naufal.githubuser.data.source.local.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ResponseGithubUser(
	@field:SerializedName("users")
	val users: List<UsersItem> = mutableListOf()
) : Parcelable