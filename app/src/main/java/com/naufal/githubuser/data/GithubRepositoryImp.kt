package com.naufal.githubuser.data

import com.naufal.githubuser.data.source.local.LocalDataSource
import com.naufal.githubuser.data.source.local.model.UsersItem
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GithubRepositoryImp @Inject constructor(private val localDataSource: LocalDataSource) :
    GithubRepository {

    override suspend fun getGithubUserList(): List<UsersItem> = localDataSource.getGithubUserList()

    override suspend fun getSearchUsers(query: String): List<UsersItem> = localDataSource.getSearchUsers(query)

}