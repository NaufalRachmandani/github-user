package com.naufal.githubuser.ui.detailuser

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.naufal.githubuser.R
import com.naufal.githubuser.data.source.local.model.UsersItem
import com.naufal.githubuser.databinding.FragmentDetailUserBinding

class DetailUserFragment : Fragment() {

    private var _binding: FragmentDetailUserBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailUserBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initiateData()
        initiateUI()
    }

    private fun initiateData() {
        val usersItems = DetailUserFragmentArgs.fromBundle(arguments as Bundle).usersArgs
        updateUI(usersItems)
    }

    private fun initiateUI() {
        binding.run {
            btnBackDetailUser.setOnClickListener {
                findNavController().navigate(R.id.action_detailUserFragment_to_userListFragment)
            }
        }
    }

    private fun updateUI(usersItem: UsersItem) {
        binding.run {
            val drawableResourceId: Int = requireContext().resources.getIdentifier(usersItem.avatar, "drawable", requireContext().packageName)
            ivDetailUser.setImageResource(drawableResourceId)
            mainCollapsing.title = usersItem.username
            containerUsername.text = usersItem.username
            containerName.text = usersItem.name
            containerCompany.text = usersItem.company
            containerFollowers.text = usersItem.follower.toString()
            containerFollowing.text = usersItem.following.toString()
            containerLocation.text = usersItem.location
            containerRepository.text = usersItem.repository.toString()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}