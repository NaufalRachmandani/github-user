package com.naufal.githubuser.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.naufal.githubuser.data.GithubRepository
import com.naufal.githubuser.data.source.local.model.UsersItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val githubRepository: GithubRepository): ViewModel() {
    private val _githubUserList = MutableLiveData<List<UsersItem>>()
    val githubUserList: LiveData<List<UsersItem>> = _githubUserList

    fun getGithubUserList() {
        viewModelScope.launch(Dispatchers.IO) {
            val response = githubRepository.getGithubUserList()
            _githubUserList.postValue(response)
        }
    }
}