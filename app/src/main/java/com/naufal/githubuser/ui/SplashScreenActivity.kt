package com.naufal.githubuser.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import splitties.activities.start

class SplashScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        start<MainActivity>()
        finish()
    }
}