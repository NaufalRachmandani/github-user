package com.naufal.githubuser.ui.common

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.naufal.githubuser.data.source.local.model.UsersItem
import com.naufal.githubuser.databinding.ItemRowUserBinding
import java.util.*

class UserListAdapter(private val context: Context, private val onClick: (UsersItem) -> Unit) :
    RecyclerView.Adapter<UserListAdapter.ViewHolder>() {

    private var dataList: List<UsersItem> = Collections.emptyList()

    fun setList(newList: List<UsersItem>) {
        this.dataList = newList
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemRowUserBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(usersItem: UsersItem) {
            with(binding) {

                val drawableResourceId: Int = context.resources.getIdentifier(usersItem.avatar, "drawable", context.packageName)
                civUser.setImageResource(drawableResourceId)

                tvUsername.text = usersItem.username
                tvFollowerNumber.text = usersItem.follower.toString()
                tvFollowingNumber.text = usersItem.following.toString()

                itemView.setOnClickListener {
                    onClick.invoke(usersItem)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemRowUserBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
            bind(dataList[position])
        }
    }

    override fun getItemCount(): Int = dataList.size
}