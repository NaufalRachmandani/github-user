package com.naufal.githubuser.ui.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.naufal.githubuser.data.GithubRepository
import com.naufal.githubuser.data.source.local.model.UsersItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(private val githubRepository: GithubRepository): ViewModel() {
    private val _searchedUsers = MutableLiveData<List<UsersItem>>()
    val searchedUsers: LiveData<List<UsersItem>> = _searchedUsers

    fun getSearchUsers(query: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = githubRepository.getSearchUsers(query)
            _searchedUsers.postValue(response)
        }
    }
}