package com.naufal.githubuser.ui.search

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.naufal.githubuser.R
import com.naufal.githubuser.databinding.FragmentSearchUsersBinding
import com.naufal.githubuser.ui.common.UserListAdapter
import com.naufal.githubuser.utils.afterTextChanged
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchUsersFragment : Fragment() {

    private var _binding: FragmentSearchUsersBinding? = null
    private val binding get() = _binding!!
    private val searchViewModel: SearchViewModel by viewModels()
    private val adapter by lazy {
        UserListAdapter(requireContext()) { usersItem ->
            val toDetailUserFragment = SearchUsersFragmentDirections.actionSearchUsersFragmentToDetailUserFragment(usersItem)
            findNavController().navigate(toDetailUserFragment)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSearchUsersBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initiateObserver()
        initiateData()
        initiateUI()
    }

    private fun initiateObserver() {
        searchViewModel.searchedUsers.observe(viewLifecycleOwner, {
            adapter.setList(it)
            showRecyclerList()
        })
    }

    private fun initiateData() {
        searchViewModel.getSearchUsers("")
        binding.etSearch.afterTextChanged {
            searchViewModel.getSearchUsers(it)
        }
    }

    private fun initiateUI() {
        binding.run {
            toolbar.btnToolbar.visibility = View.VISIBLE
            toolbar.btnToolbar.setOnClickListener {
                findNavController().navigate(R.id.action_searchUsersFragment_to_userListFragment)
            }
            toolbar.tvTitleToolbar.text = getString(R.string.search_users)
        }
    }

    private fun showRecyclerList() {
        binding.run {
            rvUser.setHasFixedSize(true)
            rvUser.layoutManager = LinearLayoutManager(requireContext())
            rvUser.adapter = adapter
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}