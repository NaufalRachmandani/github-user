package com.naufal.githubuser.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.naufal.githubuser.R
import com.naufal.githubuser.databinding.FragmentHomeBinding
import com.naufal.githubuser.ui.common.UserListAdapter
import dagger.hilt.android.AndroidEntryPoint
import splitties.resources.color

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private var binding: FragmentHomeBinding? = null
    private val homeViewModel: HomeViewModel by viewModels()

    private val adapter by lazy {
        UserListAdapter(requireContext()) { usersItem ->
            val toDetailUserFragment =
                HomeFragmentDirections.actionUserListFragmentToDetailUserFragment(usersItem)
            findNavController().navigate(toDetailUserFragment)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initiateObserver()
        initiateData()
        initiateUI()
    }

    private fun initiateObserver() {
        homeViewModel.githubUserList.observe(viewLifecycleOwner, {
            adapter.setList(it)
            showRecyclerList()
        })
    }

    private fun initiateData() {
        homeViewModel.getGithubUserList()
    }

    private fun initiateUI() {
        binding?.run {
            toolbar.btnAction.visibility = View.VISIBLE
            toolbar.btnAction.color(R.color.white)
            toolbar.btnAction.setOnClickListener {
                findNavController().navigate(R.id.action_userListFragment_to_searchUsersFragment)
            }
        }
    }

    private fun showRecyclerList() {
        binding?.run {
            rvUser.setHasFixedSize(true)
            rvUser.layoutManager = LinearLayoutManager(requireContext())
            rvUser.adapter = adapter
        }
    }

    override fun onDestroy() {
        binding = null
        super.onDestroy()
    }
}