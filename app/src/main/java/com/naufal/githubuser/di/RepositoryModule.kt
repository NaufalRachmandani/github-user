package com.naufal.githubuser.di

import com.naufal.githubuser.data.GithubRepository
import com.naufal.githubuser.data.GithubRepositoryImp
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun bindGithubRepository(githubRepositoryImp: GithubRepositoryImp): GithubRepository
}
